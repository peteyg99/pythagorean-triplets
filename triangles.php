<?php

abstract class Shape {
    private $sides = array();

    /**
     * Set a side's length for this shape and give it a key.
     * Throw's an exception if this side is an invalid length.
     *
     * @param mixed $sideKey
     * @param int $sideLength
     * @return void
     */
    protected function setSide($sideKey, $sideLength)
    {
        if (empty($sideLength)) throw new Exception("Invalid side length!");
        $this->sides[$sideKey] = $sideLength;
    }

    /**
     * Get a side's length for this shape.
     *
     * @param mixed $sideKey
     * @return int
     */
    protected function getSide($sideKey)
    {   
        return $this->sides[$sideKey];
    }
    
    /**
     * Get all side lengths for this shape.
     *
     * @return array
     */
    protected function getAllSides()
    {   
        return $this->sides;
    }
}

class Triangle extends Shape {

    /**
     * Create a new RightAngledTriangle instance.
     *
     * @param int $sideA
     * @param int $sideB
     * @param int $sideC
     * @return void
     */
    public function __construct($sideA, $sideB, $sideC)
    {
        $this->setSide('a', $sideA);
        $this->setSide('b', $sideB);
        $this->setSide('c', $sideC);
    }

    /**
     * Get side A's length.
     *
     * @return int
     */
    public function getSideA()
    {
        return $this->getSide('a');
    }

    /**
     * Get side B's length.
     *
     * @return int
     */
    public function getSideB()
    {
        return $this->getSide('b');
    }

    /**
     * Get side C's length.
     *
     * @return int
     */
    public function getSideC()
    {
        return $this->getSide('c');
    }

    /**
     * Get the sum of all side lengths.
     *
     * @return int
     */
    public function getSumOfAllSides()
    {
        return array_sum($this->getAllSides());
    }
}

class RightAngledTriangle extends Triangle {

    /**
     * Create a new RightAngledTriangle instance.
     * Throws an error if not a right-angle.
     *
     * @param int $sideA
     * @param int $sideB
     * @param int $hypotenuse
     * @return void
     */
    public function __construct($sideA, $sideB, $hypotenuse)
    {
        parent::__construct($sideA, $sideB, $hypotenuse);

        if (!$this->isRightAngle()) throw new Exception("This is not a right angle!");
    }

    /**
     * Is this triangle a right angle?
     *
     * @return int
     */
    private function isRightAngle()
    {
        return $this->getSumABSquared() === $this->getHypotenuseSquared();
    }

    /**
     * Get the sum of side lengths A squared & B squared.
     *
     * @return int
     */
    private function getSumABSquared()
    {
        return pow($this->getSideA(), 2) + pow($this->getSideB(), 2);
    }

    /**
     * Get side length A squared.
     *
     * @return int
     */
    public function getASquared()
    {
        return $this->getSquared($this->getSideA());
    }

    /**
     * Get side length B squared.
     *
     * @return int
     */
    public function getBSquared()
    {
        return $this->getSquared($this->getSideB());
    }

    /**
     * Get the hypotenuse length squared.
     *
     * @return int
     */
    public function getHypotenuseSquared()
    {
        return $this->getSquared($this->getSideC());
    }

    /**
     * Return a squared number.
     *
     * @return int
     */
    private function getSquared($num)
    {
        return pow($num, 2);
    }
}
