<?php

require_once 'triangles.php';

class PythagoreanTripletFinder {

    private $useFastTest;
    private $stopAfterMatch;

    /**
     * Create a new PythagoreanTripletFinder instance.
     *
     * @param bool $useFastTest
     * @param bool $stopAfterMatch
     * @return void
     */
    public function __construct($useFastTest = true, $stopAfterMatch = true)
    {
        $this->useFastTest = $useFastTest;
        $this->stopAfterMatch = $stopAfterMatch;
    }

    /**
     * Run test to find triplets.
     *
     * @param int $sumOfABC
     * @return array
     */
    public function runTest($sumOfABC)
    {
        $pythagoreanTriplets = array();

        for ($a = 1; $a <= ($sumOfABC - 2); $a++)
        {
            // Start b as a number greater than a, end loop if c will be less than b
            for ($b = ($a + 1); $b <= ($sumOfABC - $a - $b); $b++)
            {
                // Get value of c, if a+b+c=1000
                $c = $sumOfABC - $a - $b;

                // Calculate to see if we have found right angle
                if ($this->testForRightAngle($a, $b, $c))
                {
                    $pythagoreanTriplets[] = new RightAngledTriangle($a, $b, $c);
                    
                    // As we know there is only 1, we can break here
                    if ($this->stopAfterMatch) break(2); 
                }
            }
        }

        return $pythagoreanTriplets;
    }

    /**
    * Test for right-angle.
    *
    * @param int $a
    * @param int $b
    * @param int $c
    * @return bool
    */
    private function testForRightAngle($a, $b, $c)
    {
        return $this->useFastTest ? 
            $this->simpleTestForRightAngle($a, $b, $c) : 
            $this->solidTestForRightAngle($a, $b, $c);
    }

    /**
    * Simple (faster) test for right-angle.
    *
    * @param int $a
    * @param int $b
    * @param int $c
    * @return bool
    */
    private function simpleTestForRightAngle($a, $b, $c)
    {
        return (pow($a, 2) + pow($b, 2) === pow($c, 2));
    }

    /**
    * Solid (slower) test for right-angle.
    *
    * @param int $a
    * @param int $b
    * @param int $c
    * @return bool
    */
    private function solidTestForRightAngle($a, $b, $c)
    {
        try 
        {
            new RightAngledTriangle($a, $b, $c);
        } 
        catch (Exception $ex)
        {
            return false;
        }
        return true;
    }

}