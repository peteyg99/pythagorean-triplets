<?php

require_once 'lib.php';

$tripFinder = new PythagoreanTripletFinder();
$pythagoreanTriplets = $tripFinder->runTest(1000);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Pythagorean triplets</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    </head>

    <body>

        <div class="container">

            <h1>Pythagorean triplets</h1>

            <ul>
                <li>a < b < c</li>
                <li>a < b < c</li>
                <li>a<sup>2</sup> + b<sup>2</sup> = c<sup>2</sup></li>
            </ul>

            <hr />

            <?php if (count($pythagoreanTriplets) < 1): ?>

                <div class="alert alert-danger">
                    None found!
                </div>

            <?php else: ?>

                <?php foreach ($pythagoreanTriplets as $pt): ?>

                    <table class="table table-striped">
                       <thead>
                            <tr>
                                <th>a</th>
                                <th>b</th>
                                <th>c</th>
                                <th>a + b + c</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php print $pt->getSideA(); ?></td>
                                <td><?php print $pt->getSideB(); ?></td>
                                <td><?php print $pt->getSideC(); ?></td>
                                <td><?php print $pt->getSumOfAllSides(); ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table table-striped">
                       <thead>
                            <tr>
                                <th>a<sup>2</sup></th>
                                <th>b<sup>2</sup></th>
                                <th>a<sup>2</sup> + b<sup>2</sup></th>
                                <th>c<sup>2</sup></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php print $pt->getASquared(); ?></td>
                                <td><?php print $pt->getBSquared(); ?></td>
                                <td><?php print ($pt->getASquared() + $pt->getBSquared()); ?></td>
                                <td><?php print $pt->getHypotenuseSquared(); ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <hr />

                <?php endforeach; ?>

            <?php endif; ?>

        </div>

    </body>
</html>